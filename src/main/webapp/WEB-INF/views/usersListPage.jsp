<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Users List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<div class="generic-container">
    <%@include file="authheader.jsp" %>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Users </span></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Login</th>
                <th width="100"></th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td><a href="<c:url value='/id/${user.login}' />">${user.firstName}</a></td>
                    <td><a href="<c:url value='/id/${user.login}' />">${user.lastName}</a></td>
                    <td><a href="<c:url value='/id/${user.login}' />">${user.email}</a></td>
                    <td><a href="<c:url value='/id/${user.login}' />">${user.login}</a></td>
                    <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
                        <td><a href="<c:url value='/edit-user-${user.login}' />"
                               class="btn btn-success custom-width">edit</a>
                        </td>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ADMIN')">
                        <td><a href="<c:url value='/delete-user-${user.login}' />" class="btn btn-danger custom-width">delete</a>
                        </td>
                    </sec:authorize>
                    <c:choose>
                        <c:when test="${user.login == currentUser.login}">
                            <td>
                                <a href="<c:url value='/id/${user.login}' />" class="btn btn-warning">NOT
                                    LIKE THIS!</a>
                            </td>
                        </c:when>
                        <c:when test="${currentUser.followList.contains(user)}">
                            <td>
                                <form:form method="POST" modelAttribute="userinteger" action="/list/delFollow"
                                           class="form-horizontal">
                                    <input type="hidden" name="value" id="value" value="${user.id}"/>

                                    <input type="submit" value="Unfollow" class="btn btn-danger "/>
                                </form:form>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td>
                                <form:form method="POST" modelAttribute="userinteger" action="/list/addFollow"
                                           class="form-horizontal">
                                    <input type="hidden" name="value" id="value" value="${user.id}"/>
                                    <input type="submit" value="Follow" class="btn btn-info "/>
                                </form:form>

                            </td>

                        </c:otherwise>
                    </c:choose>
                        <%-- </c:otherwise>--%>
                        <%-- </sec:authorize>--%>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <sec>
        <sec:authorize access="hasRole('ADMIN')">
            <div class="well">
                <a href="<c:url value='/newuser'/>">Add New User</a>
            </div>
        </sec:authorize>
    </sec>
</div>
</body>
</html>