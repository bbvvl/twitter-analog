<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="authbar">
    <div class="btn-group floatRight">
        <div class="btn-block-fluid ">
            <c:choose>
                <c:when test="${!(currentUser.login==user.login)}">

                    <c:choose><c:when test="${currentUser.followList.contains(user)}">
                        <form:form method="POST" modelAttribute="userinteger" action="/id/${user.login}/delFollow"
                                   class="form-horizontal">
                            <input type="hidden" name="value" id="value" value="${user.id}"/>

                            <input type="submit" value="Unfollow" class="btn btn-danger"/>
                        </form:form>
                    </c:when>
                        <c:otherwise>
                            <form:form method="POST" modelAttribute="userinteger" action="/id/${user.login}/addFollow"
                                       class="form-horizontal">
                                <input type="hidden" name="value" id="value" value="${user.id}"/>

                                <input type="submit" value="Follow" class="btn btn-info "/>
                            </form:form>

                        </c:otherwise>
                    </c:choose>
                </c:when>
            </c:choose>
        </div>
        <div class="btn-block-fluid ">

            <a class="btn btn-warning " href="<c:url value="/list" />">Go to List</a>
            <a class="btn btn-default " href="<c:url value="/logout" />">Logout</a>
        </div>
    </div>
    <%-- <div class="btn-block-fluid floatRight">


     </div>--%>

    <span>Welcome <strong>${currentUser.login}</strong> to MessageService.</span>
</div>
