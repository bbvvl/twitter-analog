<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>This is ${currentUser.login} Page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>

</head>
<body>
<div class="generic-container">
    <%@include file="authheaderToUserPage.jsp" %>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>Message</th>
            <th>PublicDate</th>
            <th>Owner</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${messages}" var="message">
            <tr>
                <td>${message.message}</td>
                <td>${message.publicationDate}</td>
                <c:choose>
                    <c:when test="${message.id==currentUser.id}">
                        <td>ME</td>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${users}" var="user1">
                            <c:if test="${user1.id == message.id}">
                                <td>${user1.firstName}</td>
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>

            </tr>
        </c:forEach>
        </tbody>
    </table>
    <c:choose>
        <c:when test="${currentUser.login==user.login}">
            <form:form method="POST" modelAttribute="userMessage" class="form-horizontal">
                <input type="textarea" class="form-control input-sm" name="message" id="message"/>
                <br>
                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit" value="Add Message!" class="btn btn-primary btn-sm"/>
                    </div>
                </div>

            </form:form>
        </c:when>
    </c:choose>


</div>
</body>
</html>
