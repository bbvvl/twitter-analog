<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration Confirmation Page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>
<body>
<div class="generic-container">

    <div class="alert alert-success lead">
        ${success}
    </div>
    <h3>You will be redirected in 2 sec</h3>

    <c:choose>
        <c:when test="${edit}">

            <meta http-equiv="refresh" content=2;url=<c:url value='/list'/>/>

            <span class="well floatRight">
			Go to <a href="<c:url value='/list'/>">List page</a>
		</span>
        </c:when>
        <c:otherwise>
            <meta http-equiv="refresh" content=2;url=<c:url value='/login'/>/>

            <span class="well floatRight">
			Go to <a href="<c:url value='/login'/>">Login page</a>
		</span>
        </c:otherwise>
    </c:choose>

</div>
</body>

</html>