package spring.service;


import org.hibernate.Hibernate;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.jpa.UserRepository;
import spring.model.User;

import java.util.*;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public User findById(int id) {
        return repository.findOne(id);
    }

    public User findByLogin(String login) {
        return repository.findOneByLogin(login);
    }

    public void saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        repository.save(user);

    }

    /*
     * Since the method is running with Transaction, No need to call hibernate update explicitly.
     * Just fetch the entity from db and update it with proper values within transaction.
     * It will be updated in db once transaction ends.
     */
    public void updateUser(User user) {
       /* User entity = dao.findById(user.getId());
        if (entity != null) {
            entity.setLogin(user.getLogin());
            if (!user.getPassword().equals(entity.getPassword())) {
                entity.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            entity.setFirstName(user.getFirstName());
            entity.setLastName(user.getLastName());
            entity.setEmail(user.getEmail());
            entity.setUserProfiles(user.getUserProfiles());
        }*/

        saveUser(user);
    }


    public void deleteUserByLogin(String login) {

        User user = repository.findOneByLogin(login);
        for (User currentUser : repository.findAll()) {
            if(currentUser.getFollowList().contains(user)) currentUser.getFollowList().remove(user);

        }

        user.getFollowerList().clear();
        user.getFollowList().clear();

        repository.deleteByLogin(login);

    }

    public List<User> findAllUsers() {

        return repository.findAll();
    }

    public boolean isUserLoginUnique(Integer id, String login) {
        User user = findByLogin(login);
        return (user == null || ((id != null) && (Objects.equals(user.getId(), id))));
    }

}
