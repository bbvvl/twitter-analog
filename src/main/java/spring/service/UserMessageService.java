package spring.service;


import spring.model.User;
import spring.model.UserMessage;

import java.util.List;

public interface UserMessageService {

    UserMessage findById(int id);

    List<UserMessage> findMessagesById(int id);

    List<UserMessage> findAllMessages();

    List<UserMessage> findMessagesByFollowList(User u);

    void addMessage(UserMessage message, Integer id);

    void delMessage(Integer id);

    void addFollow(User user, String currentUser);

    void delFollow(User user, String currentUser);

    void removeUser(String login);
}
