package spring.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.jpa.UserMessageRepository;
import spring.model.User;
import spring.model.UserMessage;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("userMessageService")
@Transactional
public class UserMessageServiceImpl implements UserMessageService {

    @Autowired
    private UserMessageRepository userMessageRepository;

    @Autowired
    private UserService userService;

    @Override
    public UserMessage findById(int id) {

        return userMessageRepository.findOne(id);
    }

    @Override
    public List<UserMessage> findMessagesByFollowList(User u) {

        List<User> users = u.getFollowList();

        List<UserMessage> list = new ArrayList<>();
        for (User user : users) {
            list.addAll(userMessageRepository.findUserMessagesById(user.getId()));
        }

        return list;
    }

    @Override
    public List<UserMessage> findMessagesById(int id) {
        return userMessageRepository.findUserMessagesById(id);
    }

    @Override
    public List<UserMessage> findAllMessages() {
        return userMessageRepository.findAll();
    }

    @Override
    public void addMessage(UserMessage message, Integer id) {

        message.setId(id);

        userMessageRepository.save(message);
    }

    @Override
    public void delMessage(Integer id) {

        userMessageRepository.delete(id);
    }


    @Override
    public void addFollow(User user, String currentUser) {
        userService.findByLogin(currentUser).getFollowList().add(user);
    }

    @Override
    public void delFollow(User user, String currentUser) {
        if (userService.findByLogin(currentUser).getFollowList().contains(user))

            userService.findByLogin(currentUser).getFollowList().remove(user);
    }

    @Override
    public void removeUser(String login) {

        userMessageRepository.deleteById(userService.findByLogin(login).getId());

        userService.deleteUserByLogin(login);
    }
}
