package spring.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.jpa.UserProfileRepository;
import spring.model.UserProfile;

import java.util.List;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    UserProfileRepository userProfile;

    public UserProfile findById(int id) {
        return userProfile.findOne(id);
    }

    public UserProfile findByType(String type) {
        return userProfile.findByType(type);
    }

    public List<UserProfile> findAll() {
        return userProfile.findAll();
    }
}
