package spring.service;


import spring.model.User;

import java.util.List;


public interface UserService {
	
	User findById(int id);
	
	User findByLogin(String sso);
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteUserByLogin(String sso);

	List<User> findAllUsers(); 
	
	boolean isUserLoginUnique(Integer id, String sso);


}