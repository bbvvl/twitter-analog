package spring.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Data
@ToString(exclude = {"password", "followList", "followerList", "userProfiles"})
@EqualsAndHashCode(exclude = {"followList", "followerList", "userProfiles"})
@Entity
@Table(name = "APP_USER")
@Transactional
public class User implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty
    @Column(name = "LOGIN", unique = true, nullable = false)
    private String login;

    @NotEmpty
    @Column(name = "PASSWORD", nullable = false)
    private String password;


    @NotEmpty
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @NotEmpty
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @NotEmpty
    @Column(name = "EMAIL", nullable = false)
    private String email;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "SUBSCRIBER_LIST",
            joinColumns = {@JoinColumn(name = "SUBCRIBED_ID")},
            inverseJoinColumns = {@JoinColumn(name = "SUBCRIBER_ID")})
    private List<User> followList = new LinkedList<>();
    //SUBSCRIBE TO YOU
    @ManyToMany(mappedBy = "followList")
    private List<User> followerList = new LinkedList<>();


    @NotEmpty
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "APP_USER_USER_PROFILE",
            joinColumns = {@JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_PROFILE_ID")})
    private Set<UserProfile> userProfiles = new HashSet<>();

}
