package spring.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "USER_MESSAGE")
public class UserMessage implements Serializable {

    @Id
    @Column(name = "ID_MESSAGE")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMessage;

    @Column(name = "ID")
    private Integer id;

    @Column(name = "Message", nullable = false)
    private String message;


    @CreationTimestamp
    @Column(name = "PUBLICATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publicationDate;

}
