package spring.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import spring.errors.IncorrectValueException;
import spring.model.User;
import spring.model.UserInteger;
import spring.model.UserMessage;
import spring.model.UserProfile;
import spring.service.UserMessageService;
import spring.service.UserProfileService;
import spring.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserProfileService userProfileService;


    @Autowired
    private UserMessageService userMessageService;


    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;


    @Autowired
    private AuthenticationTrustResolver authenticationTrustResolver;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String userList(ModelMap model) {


        List<User> users = userService.findAllUsers();

        model.addAttribute("users", users);
        model.addAttribute("currentUser", userService.findByLogin(getPrincipal()));
        return "usersListPage";
    }

    @RequestMapping(value = "/list/addFollow", method = RequestMethod.POST)
    public String addFollow(@Valid UserInteger integer) {


        userMessageService.addFollow(userService.findById(integer.getValue()), getPrincipal());
        return "redirect:/list/";
    }

    @RequestMapping(value = "/list/delFollow", method = RequestMethod.POST)
    public String delFollow(@Valid UserInteger user) {

        userMessageService.delFollow(userService.findById(user.getValue()), getPrincipal());
        return "redirect:/list/";
    }

    @RequestMapping(value = "/id/{login}", method = RequestMethod.GET)
    public String userPage(@PathVariable("login") String login, ModelMap modelMap) {

        User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectValueException();


        List<UserMessage> messages = userMessageService.findMessagesById(user.getId());
        if (login.equals(getPrincipal())) {
            messages.addAll(userMessageService.findMessagesByFollowList(user));
            //SORT IF NEED THIS!
            // Collections.sort(messages, Comparator.comparing(UserMessage::getPublicationDate));/

        }
        modelMap.addAttribute("currentUser", userService.findByLogin(getPrincipal()));
        modelMap.addAttribute("messages", messages);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("users", userService.findAllUsers());
        return "userPage";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getUserPage(ModelMap modelMap) {
        return userPage(getPrincipal(), modelMap);
    }


    @RequestMapping(value = "/id/{login}", method = RequestMethod.POST)
    public String userPageWithMessages(@Valid UserMessage message, @PathVariable("login") String login) {

        User user = userService.findByLogin(login);
        userMessageService.addMessage(message, user.getId());
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String getUserPageWithMessage(@Valid UserMessage message) {
        return userPageWithMessages(message, getPrincipal());
    }

    @RequestMapping(value = "/id/{login}/addFollow", method = RequestMethod.POST)
    public String addFollowInUserPage(@Valid UserInteger user, @PathVariable("login") String login) {

        userMessageService.addFollow(userService.findById(user.getValue()), getPrincipal());
        return "redirect:/id/" + login;
    }

    @RequestMapping(value = "/id/{login}/delFollow", method = RequestMethod.POST)
    public String delFollowInUserPage(@Valid UserInteger user, @PathVariable("login") String login) {

        userMessageService.delFollow(userService.findById(user.getValue()), getPrincipal());
        return "redirect:/id/" + login;
    }


    @RequestMapping(value = {"/newuser"}, method = RequestMethod.GET)
    public String newUser(ModelMap model) {

        if (!isCurrentAuthenticationAnonymous()) {
            return "redirect:/list/";
        }

        User user = new User();
        model.addAttribute("user", user);
        model.addAttribute("edit", false);
        return "registrationPage";
    }


    @RequestMapping(value = {"/newuser"}, method = RequestMethod.POST)
    public String saveUser(@Valid User user, BindingResult result,
                           ModelMap model) {

        if (result.hasErrors()) {
            return "registrationPage";
        }



/*        проверить внутри страницы // чтобы не нужно было нажимать клавишу ок, для того чтобы увидеть эту ошибку
        //авто проверка с базой после ввода значения

        */


        if (!userService.isUserLoginUnique(user.getId(), user.getLogin())) {
            FieldError ssoError = new FieldError("user", "login", messageSource.getMessage("non.unique.login", new String[]{user.getLogin()}, Locale.getDefault()));
            result.addError(ssoError);
            return "registrationPage";
        }
        System.out.println("SAVE");
        userService.saveUser(user);

        model.addAttribute("success", "User " + user.getFirstName() + " " + user.getLastName() + " registered successfully");
        model.addAttribute("edit",false);
        return "registrationSuccessPage";
    }

    @RequestMapping(value = {"/edit-user-{login}"}, method = RequestMethod.GET)
    public String editUser(@PathVariable String login, ModelMap model) {
        User user = userService.findByLogin(login);
        model.addAttribute("user", user);
        model.addAttribute("edit", true);
        model.addAttribute("currentUser", userService.findByLogin(getPrincipal()));
        return "registrationPage";
    }

    @RequestMapping(value = {"/edit-user-{login}"}, method = RequestMethod.POST)
    public String updateUser(@Valid User user, BindingResult result,
                             ModelMap model, @PathVariable String login) {


        if (result.hasErrors()) {
            return "registrationPage";
        }



/*
/Uncomment below 'if block' if you WANT TO ALLOW UPDATING SSO_ID in UI which is a unique key to a User.
        if(!userService.isUserSSOUnique(user.getId(), user.getLogin())){
			FieldError ssoError =new FieldError("user","ssoId",messageSource.getMessage("non.unique.ssoId", new String[]{user.getLogin()}, Locale.getDefault()));
		    result.addError(ssoError);
			return "registration";
		}
*/


        userService.updateUser(user);

        model.addAttribute("success", "User " + user.getFirstName() + " " + user.getLastName() + " updated successfully");
        model.addAttribute("currentUser", userService.findByLogin(getPrincipal()));
        model.addAttribute("edit", true);
        return "registrationSuccessPage";
    }

    @RequestMapping(value = {"/delete-user-{login}"}, method = RequestMethod.GET)
    public String deleteUser(@PathVariable String login) {

        userMessageService.removeUser(login);
        return login.equals(getPrincipal()) ? "redirect:/logout" : "redirect:/list/";
    }


    @RequestMapping(value = "/access_denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("currentUser", userService.findByLogin(getPrincipal()));
        return "accessDeniedPage";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        if (isCurrentAuthenticationAnonymous()) {
            return "loginPage";
        } else {
            return "redirect:/list/";
        }
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            //new SecurityContextLogoutHandler().logout(request, response, auth);
            persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }

    private String getPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }

    @ModelAttribute("roles")
    public List<UserProfile> initializeProfiles() {
        return userProfileService.findAll();
    }


}