package spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.model.UserMessage;

import java.util.List;

public interface UserMessageRepository extends JpaRepository<UserMessage, Integer> {

    void  deleteById(Integer id);
    List<UserMessage> findUserMessagesByIdIn(List<Integer> list);
    List<UserMessage> findUserMessagesById(Integer id);
}
