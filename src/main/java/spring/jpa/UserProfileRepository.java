package spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.model.UserMessage;
import spring.model.UserProfile;

import java.util.List;

public interface UserProfileRepository extends JpaRepository<UserProfile, Integer> {


    UserProfile findByType(String type);
}
