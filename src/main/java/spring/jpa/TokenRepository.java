package spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.model.PersistentLogin;

public interface TokenRepository extends JpaRepository<PersistentLogin,String>{


    PersistentLogin findByUsername(String username);

};
