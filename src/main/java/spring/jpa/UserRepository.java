package spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import spring.model.User;

import javax.persistence.Id;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findOneByLogin(String login);

    void deleteByLogin(String l);


    @Query("SELECT p FROM User p JOIN FETCH p.followList WHERE p.id = (:id)")
    public User findByIdAndFetchRolesEagerly(@Param("id") int id);

    /*
        @Query("FROM APP_USER_USER_PROFILE Fetch JOIn  where dummy = ?1 ORDER BY tries ASC")
        List<User> findByIdOrderByEmail(int id);*/
    /*
    @Query("SELECT User FROM subscriber_list JOIN FETCH User ON SUBCRIBER_ID=User.id WHERE SUBCRIBEd_ID = (:ID)")
    List<User> findUsersByFollowList(int id);*/
/*
    @Query("SELECT User.id FROM subscriber_list JOIN User ON SUBCRIBER_ID=User.id WHERE SUBCRIBEd_ID = (:ID)")
    List<Integer> findIdsUsersByFollowList(int id);
*/

}
